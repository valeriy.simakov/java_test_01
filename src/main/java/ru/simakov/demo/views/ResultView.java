package ru.simakov.demo.views;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;


/**
 * View для отображения результата работы метода.
 * Используется при успешном выполнении(используется только поле code), а также при ошибке.
 */
@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResultView {
    @NonNull
    private ResultCode code;
}
