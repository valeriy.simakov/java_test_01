package ru.simakov.demo.views;

import lombok.Getter;
import lombok.Setter;
import ru.simakov.demo.entity.Client;

import java.util.List;

@Getter
@Setter
public class ClientsView {
    private List<Client> clientsList;
}
