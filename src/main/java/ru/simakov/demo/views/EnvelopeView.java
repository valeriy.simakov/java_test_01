package ru.simakov.demo.views;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EnvelopeView<T> {
    @NonNull
    private ResultView result;
    private T data;

    public EnvelopeView(ResultCode code, T data) {
        this.result = new ResultView(code);
        this.data = data;
    }

    public EnvelopeView(ResultCode code) {
        this.result = new ResultView(code);
    }

}