package ru.simakov.demo.views;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum ResultCode {
    OK,
    E000("unrecognizable error occurred","",""),
    E101( "VALIDATION_ERROR", "Ошибка валидации", "Изменения не сохранены"),
    E102("RECORD_NOT_FOUND", "Запись отсутствует", "Искомая запись не найдена в системе");

    private String systemMessage;
    private String userMsgTitle;
    private String userMsgText;

}
