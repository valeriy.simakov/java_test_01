package ru.simakov.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long clientId;
    private Long analysisId;
    private String data;
    private OrderStatus status;

    public Order() {}

    public Order(Long id) {
        super();
        this.id = id;
    }

    public Order(Long clientId, Long analysisId, String data, OrderStatus status) {
        this.clientId = clientId;
        this.analysisId = analysisId;
        this.data = data;
        this.status = status;
    }
}
