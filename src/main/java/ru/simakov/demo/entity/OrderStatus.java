package ru.simakov.demo.entity;

public enum OrderStatus {
    WAIT_REPORT,
    DONE,
    FAILED
}
