package ru.simakov.demo.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private  Long id;
    private  String name;
    private  String phoneNumber;
    private  String email;

    public Client() {}

    public Client(Long id) {
        super();
        this.id = id;
    }

    public Client(String name, String phoneNumber, String email) {
        super();
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }
}
