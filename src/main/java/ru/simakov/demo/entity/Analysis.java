package ru.simakov.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class Analysis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private int price;
    private int deadline;

    public Analysis() {}

    public Analysis(Long id) {
        super();
        this.id = id;
    }

    public Analysis(String name, int price, int deadline) {
        super();
        this.name = name;
        this.price = price;
        this.deadline = deadline;
    }
}
