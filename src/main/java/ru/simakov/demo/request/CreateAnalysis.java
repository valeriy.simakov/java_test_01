package ru.simakov.demo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateAnalysis {

    private String name;
    private int price;
    private int deadline;
}
