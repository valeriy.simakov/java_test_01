package ru.simakov.demo.request;

import lombok.Getter;
import lombok.Setter;
import ru.simakov.demo.entity.OrderStatus;

@Getter
@Setter
public class CreateOrder {

    private Long clientId;
    private Long analysisId;
    private String data;
    private OrderStatus status;
}
