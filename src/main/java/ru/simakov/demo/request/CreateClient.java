package ru.simakov.demo.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateClient {

    private  String name;
    private  String phoneNumber;
    private  String email;
}
