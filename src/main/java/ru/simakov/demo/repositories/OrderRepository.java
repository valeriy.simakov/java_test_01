package ru.simakov.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.simakov.demo.entity.Order;

public interface OrderRepository extends CrudRepository<Order, Long> {
}
