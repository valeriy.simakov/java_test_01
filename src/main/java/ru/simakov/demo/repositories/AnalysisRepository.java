package ru.simakov.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.simakov.demo.entity.Analysis;

public interface AnalysisRepository extends CrudRepository<Analysis, Long> {
}
