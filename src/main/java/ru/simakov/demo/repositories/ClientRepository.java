package ru.simakov.demo.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.simakov.demo.entity.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {
}
