package ru.simakov.demo.services;

import ru.simakov.demo.entity.Analysis;
import ru.simakov.demo.request.CreateAnalysis;

public interface AnalysisService {

    Long addAnalysis(CreateAnalysis createAnalysis);

    Analysis getAnalysisById(Long id);

    void removeAnalysisById(Long id);
}
