package ru.simakov.demo.services;

import ru.simakov.demo.entity.Client;
import ru.simakov.demo.request.CreateClient;
import ru.simakov.demo.views.ClientsView;

public interface ClientService {

    ClientsView getAllClients();

    Client getClientById(Long id);

    Long addClient(CreateClient client);

    void removeClientById(Long id);
}
