package ru.simakov.demo.services;

import ru.simakov.demo.entity.Order;
import ru.simakov.demo.request.CreateOrder;
import ru.simakov.demo.views.ClientsView;

public interface OrderService {
    Long addOrder(CreateOrder createOrder);
    ClientsView getClientsByAnalysisId(Long idAnalysis);
}
