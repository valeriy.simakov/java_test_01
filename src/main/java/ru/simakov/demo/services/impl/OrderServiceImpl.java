package ru.simakov.demo.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.simakov.demo.entity.Client;
import ru.simakov.demo.entity.Order;
import ru.simakov.demo.repositories.OrderRepository;
import ru.simakov.demo.request.CreateOrder;
import ru.simakov.demo.services.ClientService;
import ru.simakov.demo.services.OrderService;
import ru.simakov.demo.utils.OrderMapper;
import ru.simakov.demo.views.ClientsView;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientService clientService;

    private final OrderMapper orderMapper;

    @Override
    public Long addOrder(CreateOrder createOrder) {
        Order order = orderMapper.convert(createOrder);
        orderRepository.save(order);
        return order.getId();
    }

    @Override
    public ClientsView getClientsByAnalysisId(Long idAnalysis) {
        List<Order> orderList = new ArrayList<>();
        orderRepository.findAll().forEach(orderList::add);

        List<Order> orderListFiltered = orderList.stream()
                .filter(order -> order.getAnalysisId().equals(idAnalysis))
                .collect(Collectors.toList());

        List<Client> clients = new ArrayList<>();
        orderListFiltered.forEach(order -> clients.add(clientService.getClientById(order.getClientId())));

        ClientsView clientsView = new ClientsView();
        clientsView.setClientsList(clients);

        return  clientsView;
    }
}
