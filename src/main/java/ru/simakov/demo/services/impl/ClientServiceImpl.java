package ru.simakov.demo.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.simakov.demo.entity.Client;
import ru.simakov.demo.repositories.ClientRepository;
import ru.simakov.demo.request.CreateClient;
import ru.simakov.demo.services.ClientService;
import ru.simakov.demo.utils.DtoMapper;
import ru.simakov.demo.views.ClientsView;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final DtoMapper dtoMapper;

    @Override
    public ClientsView getAllClients() {
        ClientsView list = new ClientsView();
        List<Client> clients = new ArrayList<>();
        clientRepository.findAll().forEach(clients::add);
        list.setClientsList(clients);

        return list;
    }

    public Client getClientById(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Override
    public Long addClient(CreateClient createClient) {
        Client client = dtoMapper.convert(createClient);
        clientRepository.save(client);
        return client.getId();
    }

    @Override
    public void removeClientById(Long id) {
        Client client = getClientById(id);
        if (client != null)
            clientRepository.delete(client);
    }
}
