package ru.simakov.demo.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.simakov.demo.entity.Analysis;
import ru.simakov.demo.repositories.AnalysisRepository;
import ru.simakov.demo.request.CreateAnalysis;
import ru.simakov.demo.services.AnalysisService;
import ru.simakov.demo.utils.AnalysisMapper;

@Service
@RequiredArgsConstructor
public class AnalysisServiceImpl implements AnalysisService {

    private final AnalysisRepository analysisRepository;
    private final AnalysisMapper analysisMapper;

    @Override
    public Long addAnalysis(CreateAnalysis createAnalysis) {
        Analysis analysis = analysisMapper.convert(createAnalysis);
        analysisRepository.save(analysis);
        return analysis.getId();
    }

    @Override
    public Analysis getAnalysisById(Long id) {
        return analysisRepository.findById(id).orElse(null);
    }

    @Override
    public void removeAnalysisById(Long id) {
        Analysis analysis = getAnalysisById(id);
        if (analysis != null)
            analysisRepository.delete(analysis);
    }
}
