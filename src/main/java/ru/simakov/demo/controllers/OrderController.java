package ru.simakov.demo.controllers;

import ch.qos.logback.core.net.server.Client;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.simakov.demo.entity.Order;
import ru.simakov.demo.request.CreateOrder;
import ru.simakov.demo.services.OrderService;
import ru.simakov.demo.views.ClientsView;
import ru.simakov.demo.views.EnvelopeView;
import ru.simakov.demo.views.ResultCode;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
public class OrderController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(value = "/order/add/", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<Long> addClient(@RequestBody CreateOrder createOrder) {
        log.info("addOrder request received,{}", createOrder);
        return new EnvelopeView<>(ResultCode.OK, orderService.addOrder(createOrder));
    }

    @RequestMapping(value = "/order/clients/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<ClientsView> getClientsListByAnalysis(@PathVariable("id") Long id) {
        log.info("getClientsListByAnalysis request received,{}", id);
        return new EnvelopeView<>(ResultCode.OK, orderService.getClientsByAnalysisId(id));

    }
}
