package ru.simakov.demo.controllers;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.simakov.demo.entity.Client;
import ru.simakov.demo.request.CreateClient;
import ru.simakov.demo.services.ClientService;
import ru.simakov.demo.views.ClientsView;
import ru.simakov.demo.views.EnvelopeView;
import ru.simakov.demo.views.ResultCode;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
public class ClientController {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/clients", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<ClientsView> getAllClients() {
        log.info("getAllClients request received");
        return new EnvelopeView<>(ResultCode.OK, clientService.getAllClients());
    }

    @RequestMapping(value = "/clients/add/", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<Long> addClient(@RequestBody CreateClient createClient) {
        log.info("addClient request received,{}", createClient);
        return new EnvelopeView<>(ResultCode.OK, clientService.addClient(createClient));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<Client> getClient(@PathVariable("id") Long id) {
        log.info("getClient request received,{}", id);
        return new EnvelopeView<>(ResultCode.OK, clientService.getClientById(id));
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView delClient(@PathVariable("id") Long id) {
        log.info("delClient request received, {}", id);
        clientService.removeClientById(id);

        return new EnvelopeView<>(ResultCode.OK);
    }
}
