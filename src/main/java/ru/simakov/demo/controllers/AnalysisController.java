package ru.simakov.demo.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.simakov.demo.entity.Analysis;
import ru.simakov.demo.request.CreateAnalysis;
import ru.simakov.demo.services.AnalysisService;
import ru.simakov.demo.views.EnvelopeView;
import ru.simakov.demo.views.ResultCode;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RestController
public class AnalysisController {
    @Autowired
    private AnalysisService analysisService;

    @RequestMapping(value = "/analysis/add/", method = RequestMethod.POST, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<Long> addAnalysis(@RequestBody CreateAnalysis createAnalysis) {
        log.info("addAnalysis request received,{}", createAnalysis);
        return new EnvelopeView<>(ResultCode.OK, analysisService.addAnalysis(createAnalysis));
    }

    @RequestMapping(value = "/analysis/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView<Analysis> getAnalysis(@PathVariable("id") Long id) {
        log.info("getAnalysis request received,{}", id);
        return new EnvelopeView<>(ResultCode.OK, analysisService.getAnalysisById(id));
    }

    @RequestMapping(value = "/analysis/{id}", method = RequestMethod.DELETE, produces = APPLICATION_JSON_VALUE)
    public EnvelopeView delAnalysis(@PathVariable("id") Long id) {
        log.info("delAnalysis request received, {}", id);
        analysisService.removeAnalysisById(id);

        return new EnvelopeView<>(ResultCode.OK);
    }

}
