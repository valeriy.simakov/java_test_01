package ru.simakov.demo.utils;

import org.mapstruct.Mapper;
import ru.simakov.demo.entity.Analysis;
import ru.simakov.demo.request.CreateAnalysis;

@Mapper(componentModel = "spring")
public interface AnalysisMapper {
    Analysis convert(CreateAnalysis createAnalysis);
}
