package ru.simakov.demo.utils;

import org.mapstruct.Mapper;
import ru.simakov.demo.entity.Analysis;
import ru.simakov.demo.entity.Order;
import ru.simakov.demo.request.CreateAnalysis;
import ru.simakov.demo.request.CreateOrder;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    Order convert(CreateOrder createOrder);
}
