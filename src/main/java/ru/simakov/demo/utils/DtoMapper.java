package ru.simakov.demo.utils;

import org.mapstruct.Mapper;
import ru.simakov.demo.entity.Client;
import ru.simakov.demo.request.CreateClient;

@Mapper(componentModel = "spring")
public interface DtoMapper {

    public Client convert(CreateClient createClient);
}
