package ru.simakov.demo.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.simakov.demo.entity.Client;
import ru.simakov.demo.repositories.ClientRepository;
import ru.simakov.demo.request.CreateClient;
import ru.simakov.demo.services.impl.ClientServiceImpl;
import ru.simakov.demo.utils.DtoMapper;
import ru.simakov.demo.utils.DtoMapperImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {
        ClientController.class,
        ClientServiceImpl.class,
        DtoMapperImpl.class})
@WebMvcTest(ClientController.class)
public class ClientControllerTest {
    private static final Long ID1 = 1L;
    private static final String NAME1 = "NAME1";
    private static final String PHONE1 = "PHONE1";
    private static final String EMAIL1 = "EMAIL1";

    private static final Long ID2 = 2L;
    private static final String NAME2 = "NAME2";
    private static final String PHONE2 = "PHONE2";
    private static final String EMAIL2 = "EMAIL2";

    private static final Long ID3 = 3L;
    private static final String NAME3 = "NAME3";
    private static final String PHONE3 = "PHONE3";
    private static final String EMAIL3 = "EMAIL3";

    @Autowired
    private MockMvc mvc;

    @MockBean
    ClientRepository clientRepository;

    @Test
    void getAllClients() throws Exception {

        Iterable<Client> iter = new Iterable<Client>() {
            private List<Client> clients = new ArrayList<Client>() {{
                add(new Client(NAME1, PHONE1, EMAIL1));
                add(new Client(NAME2, PHONE2, EMAIL2));
                add(new Client(NAME3, PHONE3, EMAIL3));
            }};
            @Override
            public Iterator<Client> iterator() {
                return clients.iterator();
            }
        };

        given(clientRepository.findAll()).willReturn(iter);

        mvc.perform(get("/clients")
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.clientsList[0].name").value(NAME1))
                .andExpect(jsonPath("$.data.clientsList[0].phoneNumber").value(PHONE1))
                .andExpect(jsonPath("$.data.clientsList[0].email").value(EMAIL1))
                .andExpect(jsonPath("$.data.clientsList[2].name").value(NAME3))
                .andExpect(jsonPath("$.data.clientsList[2].phoneNumber").value(PHONE3))
                .andExpect(jsonPath("$.data.clientsList[2].email").value(EMAIL3));
    }

    @Test
    void getClient() throws Exception {
        Client client = new Client(NAME1, PHONE1, EMAIL1);
        Optional<Client> optionalClient = Optional.ofNullable(client);

        given(clientRepository.findById(ID1)).willReturn(optionalClient);

        mvc.perform(get("/clients/{id}", ID1)
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.name").value(NAME1))
                .andExpect(jsonPath("$.data.phoneNumber").value(PHONE1))
                .andExpect(jsonPath("$.data.email").value(EMAIL1));
    }

    @Test
    void addClient() throws Exception {
        CreateClient createClient = new CreateClient();
        createClient.setName(NAME1);
        createClient.setEmail(PHONE1);
        createClient.setPhoneNumber(PHONE1);

        mvc.perform(post("/clients/add/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(createClient)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void removeClient() throws Exception {
        Optional<Client> optionalClient = Optional.ofNullable(null);

        given(clientRepository.findById(ID1)).willReturn(optionalClient);

        mvc.perform(delete("/clients/{id}", ID1)
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk());
    }
}
