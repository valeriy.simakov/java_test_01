package ru.simakov.demo.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.simakov.demo.controllers.OrderController;
import ru.simakov.demo.entity.Client;
import ru.simakov.demo.entity.Order;
import ru.simakov.demo.entity.OrderStatus;
import ru.simakov.demo.repositories.ClientRepository;
import ru.simakov.demo.repositories.OrderRepository;
import ru.simakov.demo.request.CreateOrder;
import ru.simakov.demo.services.impl.ClientServiceImpl;
import ru.simakov.demo.services.impl.OrderServiceImpl;
import ru.simakov.demo.utils.DtoMapperImpl;
import ru.simakov.demo.utils.OrderMapperImpl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {
        OrderController.class,
        OrderServiceImpl.class,
        ClientServiceImpl.class,
        DtoMapperImpl.class,
        OrderMapperImpl.class
})
@WebMvcTest(OrderController.class)
public class OrderControllerTest {
    private static final Long ID = 1L;
    private static final Long ID_CLIENT = 1L;
    private static final Long ID_ANALYSIS = 1L;
    private static final String DATE = "DATE";
    private static final OrderStatus STATUS = OrderStatus.WAIT_REPORT;

    private static final String ID1 = "ID1";
    private static final Long ID_CLIENT1 = 1L;
    private static final Long ID_ANALYSIS1 = 1L;
    private static final String DATE1 = "DATE1";
    private static final OrderStatus STATUS1 = OrderStatus.WAIT_REPORT;

    private static final String ID2 = "ID2";
    private static final Long ID_CLIENT2 = 2L;
    private static final Long ID_ANALYSIS2 = 2L;
    private static final String DATE2 = "DATE2";
    private static final OrderStatus STATUS2 = OrderStatus.WAIT_REPORT;

    private static final Long ID3 = 3L;
    private static final Long ID_CLIENT3 = 3L;
    private static final Long ID_ANALYSIS3 = 3L;
    private static final String DATE3 = "DATE3";
    private static final OrderStatus STATUS3 = OrderStatus.WAIT_REPORT;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private OrderRepository orderRepository;

    @MockBean
    private ClientRepository clientRepository;

    @Test
    void addOrder() throws Exception {
        CreateOrder createOrder = new CreateOrder();
        createOrder.setAnalysisId(ID_ANALYSIS);
        createOrder.setClientId(ID_CLIENT);
        createOrder.setData(DATE);
        createOrder.setStatus(STATUS);

        mvc.perform(post("/order/add/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(createOrder)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void getClientsListByAnalysis() throws Exception {
        Iterable<Order> iter = new Iterable<Order>() {
            private List<Order> orders = new ArrayList<Order>() {{
                add(new Order(ID_CLIENT1, ID_ANALYSIS1, DATE1, STATUS1));
                add(new Order(ID_CLIENT2, ID_ANALYSIS2, DATE2, STATUS2));
                add(new Order(ID_CLIENT3, ID_ANALYSIS3, DATE3, STATUS3));
            }};
            @Override
            public Iterator<Order> iterator() {
                return orders.iterator();
            }
        };

        given(orderRepository.findAll()).willReturn(iter);
        given(clientRepository.findById(ID_CLIENT1)).willReturn(Optional.of(new Client(ID_CLIENT1)));
        given(clientRepository.findById(ID_CLIENT2)).willReturn(Optional.of(new Client(ID_CLIENT2)));
        given(clientRepository.findById(ID_CLIENT3)).willReturn(Optional.of(new Client(ID_CLIENT3)));

        mvc.perform(get("/order/clients/{id}", ID_ANALYSIS1)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.clientsList[0].id").value(ID_CLIENT1));
    }
}
