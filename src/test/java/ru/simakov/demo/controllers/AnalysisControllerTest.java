package ru.simakov.demo.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.simakov.demo.controllers.AnalysisController;
import ru.simakov.demo.controllers.ClientController;
import ru.simakov.demo.entity.Analysis;
import ru.simakov.demo.repositories.AnalysisRepository;
import ru.simakov.demo.request.CreateAnalysis;
import ru.simakov.demo.services.impl.AnalysisServiceImpl;

import java.util.Optional;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = {
        AnalysisController.class,
        AnalysisServiceImpl.class})
@WebMvcTest(ClientController.class)
public class AnalysisControllerTest {
    private static final Long ID = 1L;
    private static final String NAME = "NAME";
    private static final int DEADLINE = 10;
    private static final int PRICE = 10;

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnalysisRepository analysisRepository;

    @Test
    void getAnalysis() throws Exception {
        Analysis analysis = new Analysis(NAME, PRICE, DEADLINE);
        Optional<Analysis> optionalAnalysis = Optional.ofNullable(analysis);

        given(analysisRepository.findById(ID)).willReturn(optionalAnalysis);

        mvc.perform(get("/analysis/{id}", ID)
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.data.name").value(NAME))
                .andExpect(jsonPath("$.data.price").value(PRICE))
                .andExpect(jsonPath("$.data.deadline").value(DEADLINE));
    }

    @Test
    void addAnalysis() throws Exception {
        CreateAnalysis createAnalysis = new CreateAnalysis();
        createAnalysis.setName(NAME);
        createAnalysis.setDeadline(DEADLINE);
        createAnalysis.setPrice(PRICE);

        mvc.perform(post("/analysis/add/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(createAnalysis)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    void removeAnalysis() throws Exception {
        Optional<Analysis> optionalAnalysis = Optional.ofNullable(null);

        given(analysisRepository.findById(ID)).willReturn(optionalAnalysis);

        mvc.perform(delete("/analysis/{id}", ID)
                .contentType(MediaType.APPLICATION_JSON)).andDo(print())
                .andExpect(status().isOk());
    }
}
